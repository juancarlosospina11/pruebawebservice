package WsClientMain;

import java.util.Scanner;
import ws.WsPrueba;
import ws.WsPrueba_Service;

public class Main {

   
    public static void main(String[] args) {
        
        WsPrueba_Service webs = new WsPrueba_Service();
        WsPrueba clt = webs.getWsPruebaPort();
        Scanner input = new Scanner (System.in);
        
        boolean Flag=false;
        int opcion=0;
        
        String Nombre = " ";
        String tipo_doc = " ";
        String  num_doc = " ";
        
        do{
        System.out.println("|------------------------------|");
        System.out.println("|            Menú              |");
        System.out.println("|------------------------------|");
        System.out.println("|    1. Registrar usuarios     |");
        System.out.println("|    2. Consultar usuarios     |");
        System.out.println("|    3. Salir                  |");
        System.out.println("|------------------------------|");
        System.out.println("    ");
        System.out.println("--------------------");
            System.out.print("Respuesta: ");
        opcion = input.nextInt();
        System.out.println("--------------------");
        Scanner input2 = new Scanner (System.in);
        switch(opcion){
            case 1: System.out.println("|--------------------------|");
                    System.out.println("|         Registro         |");
                    System.out.println("|--------------------------|");
                    System.out.println("-------------------------------------");
                    System.out.print("-> Ingrese Nombre Completo: ");
                    Nombre = input2.nextLine();
                    
                    System.out.println("-------------------------------------");
                    System.out.print("-> ingrese Tipo de Documento (CC ó TI):");
                    tipo_doc = input2.nextLine();
                    System.out.println("-------------------------------------");
                    System.out.print("-> Ingrese Numero de Documento de Identidad: ");
                    
                    boolean esNumerico = false;
                    do{
                        System.out.print("Ingrese el Numero de documento de Identidad ");
                        System.out.print("(Valido numero de 10 digitos): ");
                        
                        num_doc = input2.nextLine();
                        
                      
                            for( int i = 0; i < num_doc.length(); i++ )
                                {
                                    if(Character.isDigit(num_doc.charAt( i )))
                                    {
                                        esNumerico = true;
                                    }else
                                    {
                                            esNumerico = false;
                                            break;
                                        
                                    }

                                }
                    }while(esNumerico == false || num_doc.length()>10);
                    
                    System.out.println("-------------------------------------");
                    System.out.println("-------------------------------------");
                    
                    int codigo = clt.registroUsuario(Nombre, tipo_doc, Integer.valueOf(num_doc));
                    
                    switch(codigo){
                        case 0: 
                            System.out.println("Registro exitoso.");
                            Flag=true;
                            break;
                        case 1:
                            System.out.println("Codigo error:"+codigo);
                            System.out.println("El tipo de documento no es valido.");
                            System.out.println("(Sólo es válido TI ó CC)");
                            Flag=true;
                            break;
                        case 2: 
                            System.out.println("Codigo error:"+codigo);
                            System.out.println("El numero de documento no debe exceder los 10 digitos.");
                            Flag=true;
                            break;
                        case 3: 
                            System.out.println("Codigo error:"+codigo);
                            System.out.println("El nombre no debe contener numeros.");
                            Flag=true;
                            break;
                        case 4: 
                            System.out.println("Codigo error:"+codigo);
                            System.out.println("El nombre está vacío.");
                            Flag=true;
                            break;
                    }
                    break;
            case 2: System.out.println("consultar");        
                    break;
                    
            case 3: Flag=false;
                    break;
        }
        
        }while(Flag == true);
        
    }
        
        
    }
    

